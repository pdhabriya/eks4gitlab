# EKS4GitLab

A barebones toolset for setting up EKS with GitLab. The tool creates a new VPC,
with a new security group, and then a new cluster, for every GitLab project or
group. Trying to use the same security group for multiple clusters in AWS, or
trying to use the same cluster for multiple projects/groups in GitLab, will
result in errors.

### Prerequisites

An AWS account with:

- Less VPCs than your limit (typically 5)
- Less t3.medium instances than your limit (limit typically 7, the script
  creates 3)
- A key pair in `us-east-1` called `gitlab-useast1-keypair`
- An IAM role set up as per the "Create your Amazon EKS Service Role" section of
  the [EKS
  instructions](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)
  with the name `gitlab-eks-role`

_NOTE: The key pair and IAM role must exist and must be named exactly right for this to work!_

The following software installed locally:

- AWS CLI
- `aws-iam-authenticator`
- `jq`
- `kubectl`
- `envsubst` - See [MacOS instructions](https://stackoverflow.com/a/37192554)

### Constraints

- Only works in `us-east-1`
- Creates 3-node clusters with t3.medium hosts
- No support for GPU acceleration
- No way to clean up after yourself

### How to use it

To use the tool, run `create-cluster.sh` and pass in the name of the project or
group (to be used as part of the cluster name etc). It will take some time, and
the script will pause when there are actions for you to take. Follow the
instructions carefully!

### Teardown

To remove everything, some combination of the following steps is necessary:

- Delete the CloudFormation stacks
- Terminate the EC2 instances
- Remove volumes??
- Delete the autoscaling group (?)
- Delete the load balancer (?)
- Delete the VPC - this sometimes hangs on in-use network interfaces

### Other Documents

Here are some documents that were useful in developing the tool

- [GitLab - Connecting and deploying to an Amazon EKS cluster](https://docs.gitlab.com/ee/user/project/clusters/eks_and_gitlab/)
- [Getting Started with Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)
- [Kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [EKS Workshop](https://eksworkshop.com/)
- [Helm](https://helm.sh/docs/helm)

### Other Tools

Here are some other tools/platforms/frameworks that might help with some or all of the same functionality.

- [Kops](https://github.com/kubernetes/kops/blob/master/README.md)
- [Kubespray](https://github.com/kubernetes-sigs/kubespray)
- [Rancher](https://rancher.com/)

### Notes

Note that the `current-context` of your `kubectl` will get set to the new cluster.

```
> kubectl config current-context
arn:aws:eks:us-east-1:244747664470:cluster/eks-cluster-2
```

Use `kubectl config` to switch between clusters.

I suggest using a namespace in the GitLab form. That makes it easy to see your cluster in `kubectl`. When you run `kubectl get namespaces`, you'll see something like this:

```
> kubectl get namespaces
NAME                  STATUS   AGE
default               Active   5d
gitlab-managed-apps   Active   3h
kube-public           Active   5d
kube-system           Active   5d
```

The `default` and `kube-*` namespaces are normal for Kubernetes. The `kube-system` namespace contains things that AWS created.

```
> kubectl get pods --namespace kube-system
NAME                       READY   STATUS    RESTARTS   AGE
aws-node-8kjlw             1/1     Running   1          3d
aws-node-kmpbh             1/1     Running   1          3d
aws-node-tltk5             1/1     Running   0          3d
coredns-7bcbfc4774-2qx9l   1/1     Running   0          5d
coredns-7bcbfc4774-w4f9m   1/1     Running   0          5d
kube-proxy-28sjf           1/1     Running   0          3d
kube-proxy-692r7           1/1     Running   0          3d
kube-proxy-hxzp5           1/1     Running   1          3d
```

The `gitlab-managed-apps` namespace holds Tiller and Ingress after installation in the GitLab UI.

```
> kubectl get pods --namespace gitlab-managed-apps
NAME                                                     READY   STATUS    RESTARTS   AGE
ingress-nginx-ingress-controller-ff666c548-b2g6w         1/1     Running   0          40m
ingress-nginx-ingress-default-backend-677b99f864-zzmc6   1/1     Running   0          40m
tiller-deploy-5469f8cb9-czdmj                            1/1     Running   0          4h
```

GitLab will display this message:

> The IP address is in the process of being assigned. Please check your Kubernetes cluster or Quotas on Google Kubernetes Engine if it takes a long time.

... but in fact you won't get an IP address, because you'll be using a CNAME instead. The `kubectl get service ingress-nginx-ingress-controller`... command is from the [GitLab EKS page](https://docs.gitlab.com/ee/user/project/clusters/eks_and_gitlab/#deploying-nginx-ingress-optional) and seems to work. As an example, I pointed a CNAME record for `*.minimal-ruby-app.examples.fpotter.com` to the long hostname ending in `us-east-1.elb.amazonaws.com` and then put `minimal-ruby-app.examples.fpotter.com` in my "Base Domain".

If you check "Default to Auto DevOps pipeline", you may get this message:

> You must add a Kubernetes cluster integration to this project with a domain in order for your deployment strategy to work correctly.

... but I think you can ignore it. Kubernetes has been set up. Just check the checkbox to use Auto Devops.

It's fun to check your namespaces after your first deployment. It looks like GitLab adds a random number to the end of the project name if you don't assign a namespace in the Kubernetes setup.

```
> kubectl get namespaces
NAME                        STATUS   AGE
default                     Active   9d
gitlab-managed-apps         Active   3d
kube-public                 Active   9d
kube-system                 Active   9d
minimal-ruby-app-10832862   Active   2m
```

Also you can see your pods. Note that Auto Devops always creates a PostgreSQL pod, even if your app doesn't use PostgreSQL. I'm guessing that's a byproduct of using Herokuish. But it's "Pending" so can be ignored, I assume.

```
> kubectl get pods --namespace minimal-ruby-app-10832862
NAME                                   READY   STATUS    RESTARTS   AGE
production-759b9cb575-n2cgp            1/1     Running   0          3m
production-postgres-5b5cf56747-5fcfk   0/1     Pending   0          3m
```

Sometimes you'll get the "should be accessible" message in the job output, even though the deployment fails afterwards. Such as this:

```
Application should be accessible at: http://fpotter-examples-minimal-ruby-app-review-1-issue-1-vriev5.minimal-ruby-app.examples.fpotter.com
Waiting for deployment "review-1-issue-1-vriev5" rollout to finish: 0 of 1 updated replicas are available...
error: deployment "review-1-issue-1-vriev5" exceeded its progress deadline
ERROR: Job failed: exit code 1
```

My best guess is that "exceeded its progress deadline" means your instances are too small (I only saw it when using t2.micro instances, before changing the script). According to the [Helm documentation](https://helm.sh/docs/helm/#helm-upgrade), the `helm update` command with the `--wait` option (which is what Auto Devops uses) respects a timeout that defaults to 5 minutes.

Here's a cool way to tell which pods are running on which nodes:

```
> kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName --namespace minimal-ruby-app-10832862
NAME                                                STATUS    NODE
production-759b9cb575-7hxw9                         Running   ip-192-168-226-39.ec2.internal
production-postgres-5b5cf56747-5fcfk                Pending   <none>
review-1-issue-1-vriev5-77bd8b5d88-8m9fc            Running   ip-192-168-145-105.ec2.internal
review-1-issue-1-vriev5-postgres-7bc599f48d-f4cgl   Pending   <none>
```

Do **not** try to use the same cluster on another project or group! GitLab will
get stuck and time out on the Helm Tiller install step.

As of this writing, group-level Kubernetes clusters work with GitLab, but do not
support Monitoring.

[Some people criticize
Helm](https://medium.com/virtuslab/think-twice-before-using-helm-25fbb18bc822).
If you want to use Auto Devlops but change the Helm behavior, copy the [Auto
Devops YAML
file](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)
to your project and edit it (or [import it and override it](https://docs.gitlab.com/ee/ci/yaml/)).

Another error message we have seen in deployment jobs is:

```
Error: UPGRADE FAILED: "staging" has no deployed releases
```

The only resolution for that seems to be blowing away the cluster and starting again.
