#!/bin/bash
set -e

[[ -z "$1" ]] && { echo "Please provide a project name" ; exit 1; }


echo "-- GETTING IAM ROLE --"
ROLE=`aws iam get-role --role-name gitlab-eks-role | jq -r ".Role.Arn"`
echo $ROLE


echo "-- CREATING VPC --"
VPCSTACK=$1-vpc-stack
VPCOUTPUT=`aws cloudformation create-stack --stack-name $VPCSTACK --template-url https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/amazon-eks-vpc-sample.yaml`
VPCSTACKID=`echo "$VPCOUTPUT" | jq -r .StackId`
echo $VPCSTACKID
echo "-- WAITING --"
aws cloudformation wait stack-create-complete --stack-name $VPCSTACKID
echo "-- OUTPUTS --"
VPCOUTPUTS=`aws cloudformation describe-stacks --stack-name $VPCSTACKID | jq -r '.Stacks | .[0] | .Outputs'`
SECGROUP=`echo $VPCOUTPUTS |  jq -r 'map(select(.OutputKey == "SecurityGroups")) | .[0] | .OutputValue'`
echo "Security Group: $SECGROUP"
VPCID=`echo $VPCOUTPUTS |  jq -r 'map(select(.OutputKey == "VpcId")) | .[0] | .OutputValue'`
echo "VPC ID: $VPCID"
SUBNETS=`echo $VPCOUTPUTS |  jq -r 'map(select(.OutputKey == "SubnetIds")) | .[0] | .OutputValue'`
echo "Subnets: $SUBNETS"


echo "-- CREATING CLUSTER --"
CLUSTER=$1-cluster
aws eks --region us-east-1 create-cluster --name $CLUSTER --role-arn $ROLE --resources-vpc-config subnetIds=$SUBNETS,securityGroupIds=$SECGROUP
echo "-- WAITING --"
aws eks wait cluster-active --name $CLUSTER
echo "-- UPDATING KUBECONFIG --"
aws eks --region us-east-1 update-kubeconfig --name $CLUSTER --alias $CLUSTER


echo "-- ADDING NODES --"
NODESTACK=$1-nodestack
SUBNETSPARAM=${SUBNETS//,/\\,}
NODEOUTPUT=`aws cloudformation create-stack --stack-name $NODESTACK --template-url https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/amazon-eks-nodegroup.yaml --parameters ParameterKey=ClusterName,ParameterValue=$CLUSTER ParameterKey=ClusterControlPlaneSecurityGroup,ParameterValue=$SECGROUP ParameterKey=NodeGroupName,ParameterValue=$CLUSTER-nodegroup ParameterKey=NodeImageId,ParameterValue=ami-0eeeef929db40543c ParameterKey=KeyName,ParameterValue=gitlab-useast1-keypair ParameterKey=VpcId,ParameterValue=$VPCID ParameterKey=Subnets,ParameterValue=$SUBNETSPARAM --capabilities CAPABILITY_IAM`
NODESTACKID=`echo $NODEOUTPUT | jq -r .StackId`
echo $NODESTACKID
echo "-- WAITING --"
aws cloudformation wait stack-create-complete --stack-name "$NODESTACKID"
echo "-- OUTPUTS --"
NODEOUTPUTS=`aws cloudformation describe-stacks --stack-name $NODESTACKID | jq -r '.Stacks | .[0] | .Outputs'`
export INSTANCEROLE=`echo $NODEOUTPUTS |  jq -r 'map(select(.OutputKey == "NodeInstanceRole")) | .[0] | .OutputValue'`
echo "Instance Role: $INSTANCEROLE"


echo "-- APPLYING ROLE TO ENABLE NODES --"
envsubst < kubectl/aws-auth-cm.yaml | kubectl apply -f -
echo "-- WAITING --"
sleep 10

echo "-- NODES --"
kubectl get nodes

echo "-- SETTING UP SECURITY --"
kubectl apply -f ./kubectl/eks-admin-service-account.yaml
kubectl apply -f ./kubectl/eks-admin-cluster-role-binding.yaml


echo "-- GETTING INFO FOR GITLAB --"
API_URL=`kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`
SECRET_NAME=`kubectl -n kube-system get secrets | grep 'gitlab-admin' | awk '{print $1;}'`
TOKEN=`kubectl -n kube-system get secret $SECRET_NAME -o jsonpath="{['data']['token']}" | base64 --decode`
CA_CERTIFICATE=`kubectl -n kube-system get secret $SECRET_NAME -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`

echo
echo "API URL:"
echo $API_URL
echo
echo "CA CERTIFICATE:"


# This is a hack to break only the certificate itself with newlines.
echo $CA_CERTIFICATE | sed "s/ CERTIFICATE/*CERTIFICATE/g" | tr " " "\n" | sed "s/*CERTIFICATE/ CERTIFICATE/g"


echo
echo "TOKEN:"
echo $TOKEN

echo
echo "1. Copy and paste the URL, CA CERTIFICATE, TOKEN, and base domain into your Kubernetes page on GitLab"
echo "2. Click the button to install Helm Tiller"
echo "3. Click the button to install Ingress"
echo "4. When steps 1-3 are complete, press Enter to continue or ctrl-C to quit"
read nothing

export hostname=`kubectl get service ingress-nginx-ingress-controller -n gitlab-managed-apps -o jsonpath="{.status.loadBalancer.ingress[0].hostname}"`
echo
echo "HOSTNAME:"
echo $hostname
echo "In your DNS provider, create a wildcard CNAME record for *.<your base domain> pointing to the HOSTNAME above"
echo "Then try running a pipline. Good luck!"
